<?php

namespace Drupal\jsonapi_pathauto_breadcrumbs\Plugin\jsonapi\FieldEnhancer;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\jsonapi_extras\Annotation\ResourceFieldEnhancer;
use Drupal\jsonapi_extras\Plugin\ResourceFieldEnhancerBase;
use Drupal\pathauto\PathautoItem;
use Shaper\Util\Context;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @ResourceFieldEnhancer(
 *   id = "breadcumbs_field_enhancer",
 *   label = @Translation("Breadcrumbs Field"),
 *   description = @Translation("Add breadcrumbs to the path alias field")
 * )
 */
final class BreadcrumbsFieldEnhancer extends ResourceFieldEnhancerBase implements ContainerFactoryPluginInterface {

  private const KEY_ALIAS = 'alias';

  private const KEY_FIELD = 'field_item_object';

  public function __construct(
    array $configuration,
    string $plugin_id,
    $plugin_definition,
    protected readonly Json $encoder,
    private readonly EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
  }

  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('serialization.json'),
      $container->get('entity_type.manager')
    );
  }

  public function defaultConfiguration(): array {
    return [
      'include_home' => FALSE,
      'include_current' => FALSE,
    ];
  }

  public function getSettingsForm(array $resource_field_info): array {
    $settings = empty($resource_field_info['enhancer']['settings'])
      ? $this->getConfiguration()
      : $resource_field_info['enhancer']['settings'];

    return [
      'include_home' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Include home'),
        '#default_value' => $settings['include_home'],
      ],
      'include_current' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Include current'),
        '#default_value' => $settings['include_current'],
      ],
    ];
  }

  protected function doTransform(mixed $data, Context $context): mixed {
    return $data;
  }

  protected function doUndoTransform(mixed $data, Context $context): mixed {
    if (!$this->formatterApplies($data, $context)) {
      return $data;
    }

    $data['breadcrumbs'] = $this->buildBreadcrumbsList(
      $data,
      $context->getArrayCopy()
    );

    return $data;
  }

  public function getOutputJsonSchema(): array {
    return [
      'oneOf' => [
        ['type' => 'object'],
        ['type' => 'array'],
        ['type' => 'null'],
      ],
    ];
  }

  private function formatterApplies(array $data, Context $context): bool {
    if (!is_array($data) || !isset($data[self::KEY_ALIAS])) {
      return FALSE;
    }

    $context_copy = $context->getArrayCopy();

    return isset($context_copy[self::KEY_FIELD])
      && $context_copy[self::KEY_FIELD] instanceof PathautoItem;
  }

  private function buildBreadcrumbsList(array $data, array $context): array {
    $breadcrumbs = [];

    if ($this->configuration['include_current']) {
      $breadcrumbs = $this->addCurrentBreadcrumb(
        $breadcrumbs,
        $context[self::KEY_FIELD]
      );
    }

    $breadcrumbs = $this->addBreadcumbsList(
      $breadcrumbs,
      $data[self::KEY_ALIAS]
    );

    if ($this->configuration['include_home']) {
      $breadcrumbs = $this->addHomeBreadcrumb($breadcrumbs);
    }

    return array_reverse($breadcrumbs);
  }

  private function addHomeBreadcrumb(array $breadcrumbs): array {
    if ($this->configuration['include_home']) {
      $breadcrumbs[] = [
        'path' => '/',
        'label' => $this->t('Home'),
      ];
    }

    return $breadcrumbs;
  }

  private function addCurrentBreadcrumb(array $breadcrumbs, PathautoItem $item): array {
    if ($this->configuration['include_current']) {
      $breadcrumbs[] = $this->createBreadcrumbForEntity($item->getEntity());
    }

    return $breadcrumbs;
  }

  private function addBreadcumbsList(array $breadcrumbs, string $alias): array {
    $parts = array_reverse(explode('/', $alias));
    while (array_shift($parts)) {
      $new_parts = array_reverse($parts);
      $path = implode('/', $new_parts);

      $breadcrumb = $this->createBreadcrumbForAlias($path);
      if (!is_null($breadcrumb)) {
        $breadcrumbs[] = $breadcrumb;
      }
    }

    return $breadcrumbs;
  }

  private function createBreadcrumbForAlias(string $alias): ?array {
    /** @var \Drupal\path_alias\Entity\PathAlias[] $aliases */
    $aliases = $this->entityTypeManager
      ->getStorage('path_alias')
      ->loadByProperties(['alias' => $alias]);

    if (empty($aliases)) {
      return NULL;
    }

    foreach ($aliases as $alias) {
      $path = $alias->getPath();
      $entity = $this->getEntityForPath($path);
      if (!$entity instanceof ContentEntityInterface) {
        continue;
      }

      return $this->createBreadcrumbForEntity($entity);
    }
  }

  private function getEntityForPath(string $path): ?ContentEntityInterface {
    @[$_, $entity_type, $entity_id] = explode('/', $path);
    if (empty($entity_type) || empty($entity_id)) {
      return NULL;
    }

    if (!$this->entityTypeManager->hasDefinition($entity_type)) {
      return NULL;
    }

    $entity = $this->entityTypeManager
      ->getStorage($entity_type)
      ->load($entity_id);

    return $entity instanceof ContentEntityInterface
      ? $entity
      : NULL;
  }

  private function createBreadcrumbForEntity(ContentEntityInterface $entity): array {
    return [
      'path' => $entity->toUrl()->toString(),
      'label' => $entity->label(),
    ];
  }

}
